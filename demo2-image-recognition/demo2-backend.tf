provider "aws" {
  region = "us-west-2"
}

resource "aws_cloudformation_stack" "demo2-backend" {
  name         = "photo-sharing-backend"
  template_url = "https://s3-us-west-2.amazonaws.com/media-sharing-refarch/cloudformation/image-processing-v3-node8.output.yaml"

  tags = {
    app = "grit"
  }

  capabilities = ["CAPABILITY_AUTO_EXPAND", "CAPABILITY_IAM"]
}

data "template_file" "config-file" {
  template = "${file("config.ts.tpl")}"

  vars = {
    CognitoIdentityPool     = "${lookup(aws_cloudformation_stack.demo2-backend.outputs, "CognitoIdentityPool")}"
    DDBAlbumMetadataTable   = "${lookup(aws_cloudformation_stack.demo2-backend.outputs, "DDBAlbumMetadataTable")}"
    DDBImageMetadataTable   = "${lookup(aws_cloudformation_stack.demo2-backend.outputs, "DDBImageMetadataTable")}"
    DescribeExecutionLambda = "${lookup(aws_cloudformation_stack.demo2-backend.outputs, "DescribeExecutionLambda")}"
    Region                  = "${lookup(aws_cloudformation_stack.demo2-backend.outputs, "Region")}"
    S3PhotoRepoBucket       = "${lookup(aws_cloudformation_stack.demo2-backend.outputs, "S3PhotoRepoBucket")}"
  }
}

resource "local_file" "config-file-out" {
  content  = data.template_file.config-file.rendered
  filename = "config.ts"
}

resource "null_resource" "empty-s3-bucket" {
  provisioner "local-exec" {
    when    = destroy
    command = "aws s3 rm s3://${lookup(aws_cloudformation_stack.demo2-backend.outputs, "S3PhotoRepoBucket")} --recursive"
  }

  depends_on = [aws_cloudformation_stack.demo2-backend]
}
