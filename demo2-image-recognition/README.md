# How to deploy this demo
#### Demo Code Provided by https://github.com/aws-samples/lambda-refarch-imagerecognition

## Running the Example
 
### Launch the CloudFormation Template in US West - Oregon (us-west-2)
Either use terraform or via the Click to Launch
```
terrform init
terraform apply
```
The backend infrastructure can be deployed in US West - Oregon (us-west-2) using the provided CloudFormation template.

Click **Launch Stack** to launch the template in the US West - Oregon (us-west-2) region in your account:

[![Launch Lambda IoT Backend into Oregon with CloudFormation](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/images/cloudformation-launch-stack-button.png)](https://console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/new?stackName=photo-sharing-backend&templateURL=https://s3-us-west-2.amazonaws.com/media-sharing-refarch/cloudformation/image-processing-v3-node8.output.yaml)

> In the last page of the wizard, make sure to click the checkboxes to accept:
> 
> * I acknowledge that AWS CloudFormation might create IAM resources.
> * I acknowledge that AWS CloudFormation might create IAM resources with custom names.
> * I acknowledge that AWS CloudFormation might require the following capability: `CAPABILITY_AUTO_EXPAND`
> * Click Create Changeset
> 
> before clicking **"Create stack"**

### Configuring the web app
The web app needs references to the resources created from the CloudFormation template above. To do so, follow these steps:

1. Go to CloudFormation [console](https://console.aws.amazon.com/cloudformation/home)
2. Go to the **Output** section of the stack you just launched in the previous section
3. Open the **Config.ts** file in the **webapp/app/** folder, and fill in the corresponding values from the CloudFormation stack output

### Launch the Web App via Docker
```
docker build -t imagerecognition-webapp .
docker run -p 3000:3000 imagerecognition-webapp
```

### Using the web app
##### Login
Pick any username to log in (This is a test app to showcase the backend so it's not using real user authentication. In an actual app, you can use Amazon Cognito to manage user sign-up and login.)

The username will be used in storing ownership metadata of the uploaded images.

##### Album list
Create new or select existing albums to upload images to.

### Cleaning Up the Application Resources

To remove all resources created by this example, do the following:

1. Delete all objects from the S3 bucket created by the CloudFormation stack.
1. Delete the CloudFormation stack or `terraform destroy`
1. Delete the CloudWatch log groups associated with each Lambda function created by the CloudFormation stack.
1. `docker image rm -f imagerecognition-webapp`
