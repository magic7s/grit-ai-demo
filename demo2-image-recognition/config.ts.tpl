export interface Config {
  Region: string;
  S3PhotoRepoBucket: string;
  DDBImageMetadataTable: string;
  DDBAlbumMetadataTable: string;
  DescribeExecutionLambda: string;
  CognitoIdentityPool: string;
}

export const CONFIG: Config = {
  DDBAlbumMetadataTable : "${DDBAlbumMetadataTable}",
  CognitoIdentityPool : "${CognitoIdentityPool}",
  Region : "${Region}",   // might be replaced if you launched the template in a different region
  DDBImageMetadataTable : "${DDBImageMetadataTable}",
  S3PhotoRepoBucket : "${S3PhotoRepoBucket}",
  DescribeExecutionLambda : "${DescribeExecutionLambda}"
};
