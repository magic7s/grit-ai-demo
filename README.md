# GRIT (Girls ReInventing Tomorrow)

**Demo for Irvine, CA Event**

This demonstration strives to show the power and potenital of Artificial Intelligence (AI) in a few simple demos.

[Slides are located here](http://bit.ly/grit-slides) and
[And a copy in the repository](GRIT-AI-Demo.pptx) 

We will go through three demonstrations:
1. [Sentiment Analysis -- IMDB Review](https://storage.googleapis.com/tfjs-examples/sentiment/dist/index.html)
2. ~~Deep Learning -- Image Recognition~~ (not currently working)
2. [Engagement Meter - Video Analysis](https://github.com/aws-samples/amazon-rekognition-engagement-meter)
3. [Play a Game - Emoji Hunt](https://emojiscavengerhunt.withgoogle.com/)

